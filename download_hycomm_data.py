from parcels import FieldSet, ParticleSet, Variable, JITParticle, AdvectionRK4, plotTrajectoriesFile
import numpy as np
import math
from datetime import timedelta,datetime
from operator import attrgetter
import xarray as xr
import pandas as pd
# exec(open("download_hycomm_data.py").read())

##- Get model data
url = 'https://tds.hycom.org/thredds/dodsC/GLBy0.08/expt_93.0/FMRC/GLBy0.08_930_FMRC_best.ncd'
dset = xr.open_dataset(url,decode_times=False)
#- Fix time 
tinfo = dset.time.attrs
attrs = tinfo['units']  #- 'hours since 2022-03-19 12:00:00.000 UTC'
units, reference_date = attrs.split(' since ')
dt0= datetime.strptime(reference_date,'%Y-%m-%d %H:%M:%S.%f UTC')
hours = dset.time.data
hours = np.array([timedelta(hours=i) for i in hours])
ctime = dt0 + hours

#lons = (dset.lon.data + 180)%360 - 180
#lats = dset.lat.data

##- Clip model to CONUS bounds
min_lat = 15
max_lat = 50 
max_lon = -65%360
min_lon = -130%360

ds = dset.sel( lat=slice(min_lat,max_lat), lon=slice(min_lon,max_lon),depth=slice(0,0))
ds['time'] = ctime


U = ds.water_u[:,0,:,:]
V = ds.water_v[:,0,:,:]

print('Creating dataset')
#U.to_netcdf('hycomm_U.nc')
#V.to_netcdf('hycomm_V.nc')

'''
ds = xr.Dataset(
    data_vars = {
        U:(['lat','lon','time'],U.data),
        V:(['lat','lon','time'],V.data),
    },
    coords={
        lat:(["lat"],lat),
        lon:(["lon"],lon),
        time:(["time"],ctime),
    },
    attrs=dset.attrs,
)

'''

## Define names of files containing U/V velocities
filenames = {'U': "hycomm_U.nc",
             'V': "hycomm_V.nc"}

## Define dict of variables and dimensions
variables = {'U': 'water_u',
             'V': 'water_v'}
dimensions = {'lat': 'lat',
              'lon': 'lon',
              'time': 'time'}

##- read in 
fieldset = FieldSet.from_netcdf(filenames, variables, dimensions)


