import netCDF4
import numpy as np
from datetime import datetime, timedelta
import csv
import simplekml
## exec(open("parcels_to_kml.py").read())


##- Try opening up remote opendap resource, from CDIPPY
def get_nc(url):
    try:
        nc = netCDF4.Dataset(url)
    except:
        nc = None
    return nc

def get_var(nc,var_name):
    if (nc is None) or (var_name not in nc.variables):
        return None
    else:
        return nc.variables[var_name]

##- Calculate distance based on Haversine formula.  
##- If performance issue, consider vector implementation instead of scalar input
def calc_dist(lon1,lat1,lon2,lat2):
    lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])
    dlon = lon2 - lon1
    dlat = lat2 - lat1 
    a = np.sin(dlat/2)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon/2)**2
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1-a))
    dist = 6371 * c
    bearing =np.arctan2(np.cos(lat1)*np.sin(lat2)-np.sin(lat1)*np.cos(lat2)*np.cos(lon2-lon1), np.sin(lon2-lon1)*np.cos(lat2)) 
    data={'distance':dist,'bearing':np.degrees(bearing)}
    return data

def calc_bearing(lon1,lat1,lon2,lat2):
    bearing =np.arctan2(cos(lat1)*np.sin(lat2)-np.sin(lat1)*np.cos(lat2)*np.cos(lon2-lon1), np.sin(lon2-lon1)*np.cos(lat2)) 
    bearing = np.degrees(bearing)

##- Passed vectors of lon,lat,time compute metrics: distance, speed, & bearing
def calc_metrics(lon,lat,time):
    data = {}
    data['lon'] = lon
    data['lat'] = lat
    data['time'] = time
    data['distance'] = [0.0]
    data['bearing'] = [0.0]
    data['speed'] = [0.0]
    for i in range(len(lon)):
        if (i > 0):
            dist_data = calc_dist(lon[i-1],lat[i-1],lon[i],lat[i])
            data['distance'].append(dist_data['distance']*1000)
            data['bearing'].append(dist_data['bearing'])
            dt = time[i] - time[i-1]
            data['speed'].append(dist_data['distance']*1000/dt.total_seconds())
    return data

##- Get a string of line coordinates
def line_coords(D):
    coords = []
    for i in range(len(D['lon'])):
        coords.append((D['lon'][i],D['lat'][i],0.0))
    return coords

def write_schema(kml,shape):
# Create a schema for extended data: heart rate, cadence and power
    schema = kml.newschema()
    schema.newgxsimplearrayfield(name='longitude', type=simplekml.Types.float, displayname='Longitude')
    schema.newgxsimplearrayfield(name='latitude', type=simplekml.Types.float, displayname='Latitude')
    schema.newgxsimplearrayfield(name='time', type=simpleTypes.string, displayname='Time')
    schema.newgxsimplearrayfield(name='distance', type=Types.float, displayname='Distance (m)')
    schema.newgxsimplearrayfield(name='speed', type=Types.float, displayname='Speed (m/s)')
    schema.newgxsimplearrayfield(name='bearing', type=Types.float, displayname='Bearing')
     

def write_kml(data):
    kml = simplekml.Kml()
    schema = kml.newschema()
    schema.newgxsimplearrayfield(name='longitude', type=simplekml.Types.float, displayname='Longitude')
    schema.newgxsimplearrayfield(name='latitude', type=simplekml.Types.float, displayname='Latitude')
    schema.newgxsimplearrayfield(name='time', type=simplekml.Types.string, displayname='Time')
    schema.newgxsimplearrayfield(name='distance', type=simplekml.Types.float, displayname='Distance (m)')
    schema.newgxsimplearrayfield(name='speed', type=simplekml.Types.float, displayname='Speed (m/s)')
    schema.newgxsimplearrayfield(name='bearing', type=simplekml.Types.float, displayname='Bearing')
     
    for i in range(len(data)):
        D = data[i]
        ## Create folder
        fol = kml.newfolder(name="Particle: "+str(i))
        ##- Add in line segment
        name = "Particle: "+str(i)
        desc = "Output from Ocean Parcels"
        coords = line_coords(D)        
        ls = fol.newlinestring(name=name,description=desc)
        ls.coords = coords
        ls.extrude = 1
        ls.altitudemode = simplekml.AltitudeMode.relativetoground 
        ##- Add points
        for j in range(len(D['lon'])):
            pt_name = "Particle @ "+D['time'][j].strftime('%Y-%m-%d %H:%MZ')
            pt_name = ""
            pt_desc = "<div>"
            pt_coords = [(D['lon'][j],D['lat'][j])]
            pnt = fol.newpoint(name=pt_name,coords=pt_coords)
            pnt.style.labelstyle.color = 'ff0000ff'
            pnt.style.scale = 1
            pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/pal4/icon56.png'
            pnt.extendeddata.schemadata.schemaurl = schema.id
            #pnt.extendeddata.schemadata.newgxsimplearraydata('longitude', "{:.2f}".format(D['lon'][j]))
            pnt.extendeddata.schemadata.newgxsimplearraydata('longitude', D['lon'][j])
        start_name = "Particle-"+str(i)+": "+D['time'][0].strftime('%Y-%m-%d %H:%MZ')
        pnt = fol.newpoint(name=start_name,coords=[(D['lon'][0],D['lat'][0])])
        pnt.style.scale = 3
        pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/pal4/icon51.png'
        pnt.style.labelstyle.color = 'ff0000ff'
        pnt.style.labelstyle.scale = 0.75 
        end_name = "Particle-"+str(i)+": "+D['time'][-1].strftime('%Y-%m-%d %H:%MZ')
        pnt = fol.newpoint(name=end_name,coords=[(D['lon'][-1],D['lat'][-1])])
        pnt.style.scale = 3
        pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/pal4/icon19.png'
        pnt.style.labelstyle.color = '9958d68d '
    return kml

def parcels_to_kml(particle_nc,out_kml):
    pfile=get_nc(particle_nc)
    ##- Get the identifier for each unique particle trajectory
    traj= np.ma.filled(pfile.variables['trajectory'], np.nan)
    lon = np.ma.filled(pfile.variables['lon'], np.nan)
    lon = (lon + 180)%360 - 180   ##- Format longitudes -180 to +180
    lat = np.ma.filled(pfile.variables['lat'], np.nan)
    time_var = pfile.variables['time']
    dtime = netCDF4.num2date(time_var[:],time_var.units)
    time = np.ma.filled(dtime, np.nan)
    pfile.close()

    ##- Iterate over diff trajectories and create Dict object
    D = []
    for i in range(len(traj)):
        data = calc_metrics(lon[i,:],lat[i,:],time[i,:])
        D.append(data)

    ##- Call function to write kml for each trajectory
    print("Creating KML: "+out_kml)
    kml = write_kml(D) 
    kml.save(out_kml)
    return D 

if __name__ == '__main__':
    fname="Output.nc"
    ofname="./kml/"+fname.replace('.nc','.kml')
    D = parcels_to_kml(fname,ofname)
